# szachy-oki
## Polish
To jest bot Discorda, który wyświetla różne informacje o klubie szachowym Olimpijskie Koło Informatyczne, ale łatwo można dostosować go do dowolnego klubu na chess.com
### Gdzie działa bot?
Bot działa sobie na Free Tier AWS EC2 na maszynie t3.micro z Ubuntu, ponieważ:
* weryfikacja oczywiście wymaga karty, ale Amazon AWS akceptuje karty przedpłacone, a takie Oracle i Google nie
* jest za darmo
* nie chcę hostować go lokalnie, bo:
    * nie bardzo mam na czym
    * nawet gdybym miau, to prąd kosztuje
## English
It is a Discord bot, what gives some info about "Olimpijskie Koło Informatyczne" chess club, but can be easily adapted to any club on chess.com
### Where does the bot run?
Bot runs on AWS EC2 Free Tier on t3.micro Ubuntu machine, because:
* Verification of course requires card payment, but AWS accepts prepaid cards
* It's free
* I don't want to host it locally, because:
    * I don't have a device for it
    * Even if I would have one, the energy isn't free
