from typing import Final
from discord import Intents, Client, Message

help_message = '''Komendy Twojego ulubionego bota:
a) listowanie adminów
Składnia: admin
b) listowanie użyszkodników
Składnia: user
c) wyświetlenie ilości użyszkodników
Składnia: how_many
d) Wyświetlenie top5 w rankingu (3 pkt za wygranie turnieju, 2 za 2 miejsce, 1 za 3 miejsce)
Składnia: leaderboard
e) dodanie wydarzenia
Składnia: schedule add YYYY.MM.DD nazwa
Uwaga: jeśli tego dnia już było inne wydarzenie, to je nadpisuje
f) usunięcie wydarzenia
Składnia: schedule remove YYYY.MM.DD
g) wylistowanie wydarzeń
Składnia: schedule list
'''

def get_response(user_input):

    lowered = user_input.lower()
    if lowered == 'help':
        return help_message

    else:
        return ''

TOKEN: Final[str] = 'INSERT_YOUR_TOKEN_HERE'

intents: Intents = Intents.default()
intents.message_content = True  # NOQA
client: Client = Client(intents=intents)

async def send_message(message: Message, user_message: str) -> None:
    if not user_message:
        print('coś się… coś się popsuło')
        return

    if is_private := user_message[0] == '?':
        user_message = user_message[1:]

    try:
        response: str = get_response(user_message)
        if str(message.channel)=='♟szachy♟':
            await message.author.send(response) if is_private else await message.channel.send(response)
    except Exception as e:
        print(e)

@client.event
async def on_ready() -> None:
    print(f'{client.user} is now running!')

@client.event
async def on_message(message: Message) -> None:
    if message.author == client.user:
        return

    username: str = str(message.author)
    user_message: str = message.content
    channel: str = str(message.channel)

    print(f'[{channel}] {username}: "{user_message}"')
    await send_message(message, user_message)

def main() -> None:
    client.run(token=TOKEN)


if __name__ == '__main__':
    main()
