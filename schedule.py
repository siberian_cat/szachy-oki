from typing import Final
from discord import Intents, Client, Message

schedule = {}


def get_response(user_input):

    lowered = user_input.lower()
    if lowered[:12] == 'schedule add':
        message = 'Dodano wydarzenie'
        komenda = lowered.split()
        schedule[komenda[2]] = komenda[3]
        return message

    elif lowered[:15]=='schedule remove':
        message = 'Usunięto wydarzenie'
        komenda = lowered.split()
        try:
            del schedule[komenda[2]]
        except:
            message = "Nic nie ma tego dnia"
        return message
    
    elif lowered=='schedule list':
        message = ""
        schedule2 = dict(sorted(schedule.items()))
        for key, value in schedule2.items():
            message += str(key)
            message += ": "
            message += str(value)
            message += "\n"
        return message

    else:
        return ''

TOKEN: Final[str] = 'INSERT_YOUR_TOKEN_HERE'

intents: Intents = Intents.default()
intents.message_content = True  # NOQA
client: Client = Client(intents=intents)

async def send_message(message: Message, user_message: str) -> None:
    if not user_message:
        print('coś się… coś się popsuło')
        return

    if is_private := user_message[0] == '?':
        user_message = user_message[1:]

    try:
        response: str = get_response(user_message)
        if str(message.channel)=='♟szachy♟':
            await message.author.send(response) if is_private else await message.channel.send(response)
    except Exception as e:
        print(e)

@client.event
async def on_ready() -> None:
    print(f'{client.user} is now running!')

@client.event
async def on_message(message: Message) -> None:
    if message.author == client.user:
        return

    username: str = str(message.author)
    user_message: str = message.content
    channel: str = str(message.channel)

    print(f'[{channel}] {username}: "{user_message}"')
    await send_message(message, user_message)

def main() -> None:
    client.run(token=TOKEN)


if __name__ == '__main__':
    main()
