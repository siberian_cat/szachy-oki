import json
import urllib3
from discord.ext import commands
from typing import Final
from discord import Intents, Client, Message

punkty_uzyszkodnikow = {"marek2011" : 5, "mmm600" : 3, "mr_catmeister" : 2, "rkubapl" : 1, "k0dziu":1}

def get_response(user_input):
    lowered = user_input.lower()

    url = 'https://api.chess.com/pub/club/olimpijskie-kolo-informatyczne'
    url2 = 'https://api.chess.com/pub/club/olimpijskie-kolo-informatyczne/members'
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    data = response.data.decode('utf-8')
    data2 = http.request('GET', url2).data.decode('utf-8')


    dane_klubu = json.loads(data)
    czlonkowie = json.loads(data2)

    lista_uzyszkodnikow = []

    for user in czlonkowie['weekly']:
        lista_uzyszkodnikow.append(user['username'])
        punkty_uzyszkodnikow[user['username']] = punkty_uzyszkodnikow.get(user['username'], 0)

    for user in czlonkowie['all_time']:
        lista_uzyszkodnikow.append(user['username'])
        punkty_uzyszkodnikow[user['username']] = punkty_uzyszkodnikow.get(user['username'], 0)

    admini = dane_klubu["admin"]
    lista_adminow = []
    for admin in admini:
        ost_slash = admin.rfind("/")
        ost_slash += 1
        lista_adminow.append(admin[ost_slash::])
        ilu_ludzi = dane_klubu["members_count"]

    if (lowered == ''):
        return 'Weź coś powiedz'
    
    elif lowered=='admin':
        message = ''
        for admin in lista_adminow:
            message += admin
            message += "\n"
        return message
    
    elif lowered=='user':
        message = ''
        for user in lista_uzyszkodnikow:
            message += user
            message += "\n"
        return message
    
    elif lowered=='how_many':
        return ilu_ludzi
    
    elif 'add_points' in lowered:
        global lista 
        lista = lowered.split()
        return "Punkty zaktualizowane"
    
    elif lowered == 'leaderboard':
        message = ''
        i = 0
        leaders = []
        for leader, points in punkty_uzyszkodnikow.items():
            leaders.append((points, leader))
        leaders.sort(reverse=True)
        for pair in leaders:
            message += pair[1]
            message += ": "
            message += str(pair[0])
            message += "\n"
            i += 1
            if (i==5):
                break
        return message

    else:
        return ''

TOKEN: Final[str] = 'INSERT_YOUR_TOKEN_HERE'

intents: Intents = Intents.default()
intents.message_content = True  # NOQA
client: Client = Client(intents=intents)

async def send_message(message: Message, user_message: str) -> None:
    if not user_message:
        print('coś się… coś się popsuło')
        return

    if is_private := user_message[0] == '?':
        user_message = user_message[1:]

    try:
        response: str = get_response(user_message)
        if (response=="Punkty zaktualizowane" and str(message.author)=="meeeooow"):
            punkty_uzyszkodnikow[lista[1]] += int(lista[2])
            print(punkty_uzyszkodnikow[lista[1]])
        elif response=='Punkty zaktualizowane':
            response = 'Tylko @Meeeooow może to robić'
        if str(message.channel)=='♟szachy♟':
            await message.author.send(response) if is_private else await message.channel.send(response)
    except Exception as e:
        print(e)

@client.event
async def on_ready() -> None:
    print(f'{client.user} is now running!')

@client.event
async def on_message(message: Message) -> None:
    if message.author == client.user:
        return

    username: str = str(message.author)
    user_message: str = message.content
    channel: str = str(message.channel)

    print(f'[{channel}] {username}: "{user_message}"')
    await send_message(message, user_message)

def main() -> None:
    client.run(token=TOKEN)


if __name__ == '__main__':
    main()